import Lib
import Test.Hspec
import Test.QuickCheck

main :: IO ()
main = hspec $ do
  describe "ascending_sub_list, version 1" $ do
    it "passes Mona's test case" $
      ascending_sub_list [1,1,2,4,2,3,4,0,5,3] `shouldBe` [[1,1,2,4],[2,3,4],[0,5],[3]]
    it "preserves elements and their order" $
      property (
        let
          element_preserved :: [Integer] -> Bool
          element_preserved list = concat (ascending_sub_list list) == list
        in element_preserved
      )
    it "is lazy" $ do
      head (ascending_sub_list $ [1,0] ++ undefined) `shouldBe` [1]
      take 2 (ascending_sub_list $ cycle [2,3,1]) `shouldBe` [[2,3],[1,2,3]]
  describe "ascending_sub_list version 2" $ do
    it "passes Mona's test case" $
      ascending_sub_list2 [1,1,2,4,2,3,4,0,5,3] `shouldBe` [[1,1,2,4],[2,3,4],[0,5],[3]]
    it "preserves elements and their order" $
      property (
        let
          element_preserved :: [Integer] -> Bool
          element_preserved list = concat (ascending_sub_list2 list) == list
        in element_preserved
      )
    it "is lazy" $ do
      head (ascending_sub_list2 $ [1,0] ++ undefined) `shouldBe` [1]
      take 2 (ascending_sub_list2 (cycle [2,3,1])) `shouldBe` [[2,3],[1,2,3]]

-- screenshot of the results at https://gitlab.com/rdnz/ascending-sub-list/raw/master/test_result.png
