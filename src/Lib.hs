module Lib
    ( ascending_sub_list
    , ascending_sub_list2
    ) where

import Data.Tuple (swap)

-- Version 1
ascending_sub_list :: Ord e => [e] -> [[e]]
ascending_sub_list [] = []
ascending_sub_list list = ascending_sub_list_first_result : ascending_sub_list rest
  where (ascending_sub_list_first_result, rest) = ascending_sub_list_first list

ascending_sub_list_first :: Ord e => [e] -> ([e], [e])
ascending_sub_list_first [] = ([], [])
ascending_sub_list_first (list_head:list_tail) =
  swap_map (list_head :) $ ascending_sub_list_continuation list_head list_tail
  where
    ascending_sub_list_continuation :: Ord e => e -> [e] -> ([e], [e])
    ascending_sub_list_continuation previous continuation_potential
      | continuation_potential_head : continuation_potential_tail <- continuation_potential,
        previous <= continuation_potential_head
        = swap_map (continuation_potential_head :)
          $ ascending_sub_list_continuation continuation_potential_head continuation_potential_tail
      | otherwise = ([], continuation_potential)

swap_map f = swap . fmap f . swap

-- Version 2
ascending_sub_list2 :: Ord element => [element] -> [[element]]
ascending_sub_list2 = foldr append []
  where
    append :: Ord element => element -> [[element]] -> [[element]]
    append element_new sub_list_list
      | sub_list_list_head : sub_list_list_tail <- sub_list_list,
        element_new <= head sub_list_list_head
        = (element_new : sub_list_list_head) : sub_list_list_tail
      | otherwise = [element_new] : sub_list_list
